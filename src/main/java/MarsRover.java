import static java.lang.Integer.parseInt;

public class MarsRover {

    private final Position position;
    private Orientation orientation;

    public MarsRover(String initialPosition, String initialOrientation) {
        String[] strings = initialPosition.split(" ");
        position = new Position(parseInt(strings[0]), parseInt(strings[1]));
        orientation = Orientation.valueOf(initialOrientation);
    }

    public void execute(String commands) {
        commands.chars().mapToObj(c -> (char) c).map(Command::of).forEach(cmd -> cmd.move(this));
    }

    public void rotateRight() {
        change(orientation.next());
    }

    public void change(Orientation orientation) {
        this.orientation = orientation;
    }

    public void moveForward() {
        orientation.move(position);
    }

    public void rotateLeft() {
        change(this.orientation.pre());
    }

    public String getPositionAndOrientation() {
        return position + " " + orientation.getCode();
    }
}
