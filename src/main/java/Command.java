public abstract class Command {

    public static Command of(char cmd) {
        if (cmd == 'R') {
            return new RCmd();
        } else if (cmd == 'L') {
            return new LCmd();
        }
        return new MCmd();
    }

    public abstract void move(MarsRover marsRover);
}
