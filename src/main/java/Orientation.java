public enum Orientation {
    N("N") {
        @Override
        public void move(Position position) {
            position.go(new int[]{0, 1});
        }
    },
    E("E") {
        @Override
        public void move(Position position) {
            position.go(new int[]{1, 0});
        }
    },
    S("S") {
        @Override
        public void move(Position position) {
            position.go(new int[]{1, 0});
        }
    },
    W("W") {
        @Override
        public void move(Position position) {
            position.go(new int[]{-1, 0});
        }
    };

    private final String code;

    Orientation next() {
        return values()[(ordinal() + 1) % values().length];
    }

    Orientation pre() {
        return values()[(values().length + ordinal() - 1) % values().length];
    }

    Orientation(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public abstract void move(Position position);
}
