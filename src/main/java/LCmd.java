public class LCmd extends Command {
    @Override
    public void move(MarsRover marsRover) {
        marsRover.rotateLeft();
    }
}
