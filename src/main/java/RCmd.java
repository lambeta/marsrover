public class RCmd extends Command {
    @Override
    public void move(MarsRover marsRover) {
        marsRover.rotateRight();
    }
}
