public class Position {

    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void go(int[] vector) {
        this.x += vector[0];
        this.y += vector[1];
    }

    @Override
    public String toString() {
        return String.format("%d %d", x, y);
    }
}
